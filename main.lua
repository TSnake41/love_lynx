package.path = package.path .. ";../?.lua;../?/init.lua"

local lynx = require "lynx"
lynx.button = require "lynx.button"
lynx.text = require "lynx.text"
lynx.slider = require "lynx.slider"
lynx.list = require "lynx.list"

local lynx_funcs = require "lynx.love-lynx"

local input = require "lib.baton".new {
  controls = {
    left = {'key:left', 'key:a', 'axis:leftx-', 'button:dpleft'},
    right = {'key:right', 'key:d', 'axis:leftx+', 'button:dpright'},
    up = {'key:up', 'key:w', 'axis:lefty-', 'button:dpup'},
    down = {'key:down', 'key:s', 'axis:lefty+', 'button:dpdown'},
    enter = {'key:return', 'button:a'},
  },
  pairs = {
    move = {'left', 'right', 'up', 'down'}
  },
  joystick = love.joystick.getJoysticks()[1],
}

local menu = {}

function love.load()
  menu = lynx.menu({
    lynx.text "Sample text 1",
    lynx.text "Sample text 2",
    lynx.text "Sample text 3",
    lynx.button("Simple hello button", function () print "Hello World !" end, {})
  }, {
    viewport = { 0, 0, 300, 200 },
    offset = { 0, 50 },
    default_height = 20,
    funcs = lynx_funcs
  })
end

function love.update(dt)
  love.window.setTitle(string.format("FPS: %d | Memory : %g kb",
    love.timer.getFPS(), math.floor(collectgarbage 'count')))

  input:update()

  for k,v in pairs(input.config.controls) do

    if input:pressed(k) then
      menu:input_key(k, "pressed")
    end

    if input:down(k) then
      menu:input_key(k, "down")
    end

  end

  menu:update(dt)
end

function love.draw()
  menu:draw()
end

function love.mousemoved(x, y)
  menu:input_mouse(x, y, 0)
end

function love.mousepressed(x, y, btn)
  menu:input_mouse(x, y, btn)
end
