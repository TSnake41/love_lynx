function love.conf(t)
    t.window.width = 300
    t.window.height = 200
    t.window.fullscreeen = true
    t.window.vsync = false
end
